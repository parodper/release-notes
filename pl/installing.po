# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2017-06-15 13:23+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "pl"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "System instalacyjny"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""
"Instalator Debiana (Debian Installer) jest oficjalnym systemem instalacji w "
"przypadku Debiana. Oferuje on wiele metod instalacji. To, które z nich są "
"dostępne, zależy od używanej architektury."

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"Obrazy instalatora wydania &releasename; są dostępne razem z przewodnikiem "
"po instalacji na <ulink url=\"&url-installer;\">stronach Debiana</ulink>."

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
#, fuzzy
#| msgid ""
#| "The Installation Guide is also included on the first CD/DVD of the "
#| "official Debian CD/DVD sets, at:"
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Przewodnik po instalacji jest również dołączony do pierwszego CD/DVD "
"oficjalnego zestawu CD/DVD Debiana w:"

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>język</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"Można również zapoznać się z <ulink url=\"&url-installer;index#errata"
"\">erratą</ulink> programu debian-installer, aby poznać listę znanych błędów."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "Co nowego w systemie instalacyjnym?"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
#, fuzzy
#| msgid ""
#| "There has been a lot of development on the Debian Installer since its "
#| "previous official release with &debian; &oldrelease;, resulting in both "
#| "improved hardware support and some exciting new features."
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"W instalator Debiana włożono wiele pracy od jego ostatniego oficjalnego "
"wydania wraz z dystrybucją &debian; &oldrelease; dzięki czemu zwiększyła się "
"obsługa sprzętu oraz dodano wiele nowych, interesujących funkcji."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
#, fuzzy
#| msgid ""
#| "In these Release Notes we'll only list the major changes in the "
#| "installer.  If you are interested in an overview of the detailed changes "
#| "since &oldreleasename;, please check the release announcements for the "
#| "&releasename; beta and RC releases available from the Debian Installer's "
#| "<ulink url=\"&url-installer-news;\">news history</ulink>."
msgid ""
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""
"W niniejszych uwagach do wydania zamieszczamy jedynie główne zmiany w "
"instalatorze. Szczegółowy przegląd zmian od chwili wydania &oldreleasename; "
"znajduje w ogłoszeniach wydań beta i RC dystrybucji &releasename;, w <ulink "
"url=\"&url-installer-news;\">archiwalnych wiadomościach</ulink> instalatora "
"Debiana."

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:120
msgid "Automated installation"
msgstr "Instalacja automatyczna"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:122
msgid ""
"Some changes mentioned in the previous section also imply changes in the "
"support in the installer for automated installation using preconfiguration "
"files.  This means that if you have existing preconfiguration files that "
"worked with the &oldreleasename; installer, you cannot expect these to work "
"with the new installer without modification."
msgstr ""
"Część zmian opisanych w poprzednim rozdziale implikuje również zmiany w "
"obsłudze automatycznych instalacji, za pomocą wstępnie skonfigurowanych "
"plików. Oznacza to, że jeśli posiada się wstępnie skonfigurowane pliki "
"działające z wydaniem &oldreleasename; instalatora, nie należy oczekiwać, że "
"będą również działać z nowym instalatorem bez potrzeby modyfikacji."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:129
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
"<ulink url=\"&url-install-manual;\">Przewodnik po instalacji</ulink> zawiera "
"zaktualizowany dodatek opisujący szczegółowo używanie wstępnej konfiguracji."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:138
#, fuzzy
#| msgid "Automated installation"
msgid "Cloud installations"
msgstr "Instalacja automatyczna"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:140
msgid ""
"The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
"bullseye for several popular cloud computing services including:"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:147
msgid "OpenStack"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:152
msgid "Amazon Web Services"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:157
msgid "Microsoft Azure"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:164
msgid ""
"Cloud images provide automation hooks via cloud-init and prioritize fast "
"instance startup using specifically optimized kernel packages and grub "
"configurations.  Images supporting different architectures are provided "
"where appropriate and the cloud team endeavors to support all features "
"offered by the cloud service."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:173
msgid ""
"More details are available at <ulink url=\"&url-cloud;\">cloud.debian.org</"
"ulink> and <ulink url=\"&url-cloud-wiki;\">on the wiki</ulink>."
msgstr ""

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:180
msgid "Container and Virtual Machine images"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:182
msgid ""
"Multi-architecture Debian bullseye container images are available on <ulink "
"url=\"&url-docker-hub;\">Docker Hub</ulink>.  In addition to the standard "
"images, a <quote>slim</quote> variant is available that reduces disk usage."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:188
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published to "
"<ulink url=\"&url-vagrant-cloud;\">Vagrant Cloud</ulink>."
msgstr ""

#~ msgid "Major changes"
#~ msgstr "Główne zmiany"

#~ msgid "Removed ports"
#~ msgstr "Usunięte porty"

#~ msgid ""
#~ "Support for the <literal>powerpc</literal> architecture has been removed."
#~ msgstr "Usunięto obsługę architektury <literal>powerpc</literal>."

#~ msgid "New ports"
#~ msgstr "Nowe porty"

#~ msgid ""
#~ "Support for the <literal>mips64el</literal> architecture has been added "
#~ "to the installer."
#~ msgstr ""
#~ "Instalator obsługuje teraz architekturę <literal>mips64el</literal>."

#~ msgid "Graphical installer"
#~ msgstr "Instalator graficzny"

#~ msgid ""
#~ "The graphical installer is now the default on supported platforms.  The "
#~ "text installer is still accessible from the very first menu, or if the "
#~ "system has limited capabilities."
#~ msgstr ""
#~ "Instalator graficzny jest obecnie domyślny na obsługiwanych platformach. "
#~ "Instalator tekstowy jest wciąż dostępny z pierwszego menu oraz na "
#~ "systemach z ograniczonymi możliwościami."

#~ msgid "The kernel flavor has been bumped to <literal>i686</literal>"
#~ msgstr "Zmieniono odmianę jądra na <literal>i686</literal>"

#~ msgid ""
#~ "The kernel flavor <literal>i586</literal> has been renamed to "
#~ "<literal>i686</literal>, since <literal>i586</literal> is no longer "
#~ "supported."
#~ msgstr ""
#~ "Zmieniono nazwę odmiany jądra z <literal>i586</literal> na <literal>i686</"
#~ "literal>, ponieważ <literal>i586</literal> nie jest już obsługiwana."

#~ msgid "Desktop selection"
#~ msgstr "Wybór środowiska"

#~ msgid ""
#~ "Since jessie, the desktop can be chosen within tasksel during "
#~ "installation, and several desktops can be selected at the same time."
#~ msgstr ""
#~ "Od wydania jessie środowiska graficzne można wybrać podczas instalacji za "
#~ "pomocą tasksela, przy czym można zainstalować kilka jednocześnie."

#~ msgid "New languages"
#~ msgstr "Nowe języki"

#~ msgid ""
#~ "Thanks to the huge efforts of translators, &debian; can now be installed "
#~ "in 75 languages, including English.  Most languages are available in both "
#~ "the text-based installation user interface and the graphical user "
#~ "interface, while some are only available in the graphical user interface."
#~ msgstr ""
#~ "Dzięki staraniom tłumaczy, &debian; może być teraz zainstalowany w 75 "
#~ "językach, w tym w języku polskim. Większość języków jest dostępnych "
#~ "zarówno w interfejsie tekstowych jak i graficznych, jednak kilka wymaga "
#~ "skorzystania z interfejsu graficznego."

#~ msgid ""
#~ "The languages that can only be selected using the graphical installer as "
#~ "their character sets cannot be presented in a non-graphical environment "
#~ "are: Amharic, Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, "
#~ "Khmer, Malayalam, Marathi, Nepali, Punjabi, Tamil, Telugu, Tibetan, and "
#~ "Uyghur."
#~ msgstr ""
#~ "Następujące języki mogą być wybrane wyłącznie w instalatorze graficznym, "
#~ "ponieważ ich znaki nie mogą być wyświetlone w interfejsie tekstowym: "
#~ "amharski, bengalski, dzongkha, gujarati, hindi, gruziński, kannada, "
#~ "khmerski, malajski, marathi, nepalski, pendżabski, tamilski, telugu, "
#~ "tybetański i ujgurski."

#~ msgid "UEFI boot"
#~ msgstr "Rozruch z UEFI"

#~ msgid ""
#~ "The &releasename; installer improves support for a lot of UEFI firmware "
#~ "and also supports installing on 32-bit UEFI firmware with a 64-bit kernel."
#~ msgstr ""
#~ "W instalatorze wydania &releasename; wiele UEFI zyskało "
#~ "poprawioną obsługę, podobnie jak możliwa jest instalacja na 32-bitowym "
#~ "oprogramowaniu UEFI 64-bitowego jądra."

#~ msgid "Note that this does not include support for UEFI Secure Boot."
#~ msgstr "Bezpieczny rozruch UEFI (UEFI Secure Boot) nie jest obsługiwany."

#~ msgid "New method for naming network interfaces"
#~ msgstr "Nowa metoda nazywania interfejsów sieciowych"

#~ msgid ""
#~ "The installer and the installed systems use a new standard naming scheme "
#~ "for network interfaces.  <literal>ens0</literal> or <literal>enp1s1</"
#~ "literal> (ethernet)  or <literal>wlp3s0</literal> (wlan) will replace the "
#~ "legacy <literal>eth0</literal>, <literal>eth1</literal>, etc.  See <xref "
#~ "linkend=\"new-interface-names\"/> for more information."
#~ msgstr ""
#~ "Instalator i zainstalowany system używają nowych reguł nazywania "
#~ "interfejsów sieciowych. Nazwy <literal>ens0</literal> lub "
#~ "<literal>enp1s1</literal> (ethernet) albo <literal>wlp3s0</literal> "
#~ "(wlan) zastąpią dawne <literal>eth0</literal>, <literal>eth1</literal> "
#~ "itp. Więcej informacji w rozdziale <xref linkend=\"new-interface-names\"/"
#~ ">."

#~ msgid "Multi-arch images now default to <literal>amd64</literal>"
#~ msgstr "Obrazy multi-arch instalują domyślnie <literal>amd64</literal>"

#~ msgid ""
#~ "Since 64-bit PCs have become more common, the default architecture on "
#~ "multi-arch images is now <literal>amd64</literal> instead of "
#~ "<literal>i386</literal>."
#~ msgstr ""
#~ "64-bitowe komputery są najpopularniejsze, dlatego domyślną architekturą "
#~ "obrazów multi-arch jest obecnie <literal>amd64</literal> zamiast "
#~ "<literal>i386</literal>."

#~ msgid "Full CD sets removed"
#~ msgstr "Usunięto pełny zestaw płyt CD"

#~ msgid ""
#~ "The full CD sets are not built anymore. The DVD images are still "
#~ "available as well as the netinst CD image."
#~ msgstr ""
#~ "Pełny zestaw płyt CD nie jest już budowany. Obrazy DVD są wciąż dostępne, "
#~ "podobnie jak obraz CD netinst."

#~ msgid ""
#~ "Also, as the installer now gives an easy choice of desktop selection "
#~ "within tasksel, only Xfce CD#1 remains as a single-CD desktop system."
#~ msgstr ""
#~ "Instalator umożliwia obecnie łatwy wybór środowiska graficznego za pomocą "
#~ "tasksel, dlatego tylko Xfce jest dostępne jako system z pojedynczej płyty "
#~ "CD."

#~ msgid "Accessibility in the installer and the installed system"
#~ msgstr "Dostępność w instalatorze i zainstalowanym systemie"

#~ msgid ""
#~ "The installer produces two beeps instead of one when booted with grub, so "
#~ "users can tell that they have to use the grub method of editing entries."
#~ msgstr ""
#~ "Przy rozruchu z użyciem grub instalator wydaje teraz dwa piknięcia "
#~ "zamiast jednego, dzięki czemu użytkownik wie, że do edycja wpisów "
#~ "konieczne jest stosowanie metody grub."

#~ msgid ""
#~ "MATE desktop is the default desktop when brltty or espeakup is used in "
#~ "debian-installer."
#~ msgstr ""
#~ "Jeśli w instalatorze korzysta się z brltty lub espeakup, to domyślnym "
#~ "środowiskiem graficznym jest MATE."

#~ msgid "Added HTTPS support"
#~ msgstr "Dodano obsługę HTTPS"

#~ msgid ""
#~ "Support for HTTPS has been added to the installer, enabling downloading "
#~ "of packages from HTTPS mirrors."
#~ msgstr ""
#~ "Do instalatora dodano obsługę HTTPS, dzięki czemu możliwe jest pobieranie "
#~ "pakietów z serwerów lustrzanych HTTPS."

#~ msgid ""
#~ "Support for the 'ia64' and 'sparc' architectures has been dropped from "
#~ "the installer since they have been removed from the archive."
#~ msgstr ""
#~ "Porzucono obsługę architektur \"ia64\" i \"sparc\" z instalatora, "
#~ "ponieważ usunięto je z archiwum."

#~ msgid "New default init system"
#~ msgstr "Nowy domyślny system init"

#~ msgid ""
#~ "The installation system now installs systemd as the default init system."
#~ msgstr ""
#~ "Instalator instaluje obecnie systemd jako nowy domyślny system init."

#~ msgid "Replacing \"--\" by \"---\" for boot parameters"
#~ msgstr "Zastąpienie \"--\" przez \"---\" w parametrach rozruchowych"

#~ msgid ""
#~ "Due to a change on the Linux kernel side, the \"---\" separator is now "
#~ "used instead of the historical \"--\" to separate kernel parameters from "
#~ "userland parameters."
#~ msgstr ""
#~ "Z powodu zmian po stronie jądra Linux, do oddzielania parametrów jądra od "
#~ "parametrów przestrzeni użytkownika obecnie używany jest separator \"---\" "
#~ "zamiast starego \"--\"."

#~ msgid "Languages added in this release:"
#~ msgstr "W niniejszym wydaniu dodano następujące języki:"

#~ msgid "Tajik has been added to the graphical and text-based installer."
#~ msgstr "Do instalatora graficznego i tekstowego dodano język tadżycki."

#~ msgid ""
#~ "Welsh has been re-added to the graphical and text-based installer (it had "
#~ "been removed in &oldreleasename;)."
#~ msgstr ""
#~ "Do instalatora tekstowego i graficznego ponownie dodano język walijski "
#~ "(usunięty w wydaniu &oldreleasename;)."

#~ msgid "Software speech support"
#~ msgstr "Obsługa syntezatorów mowy"

#~ msgid ""
#~ "&debian; can be installed using software speech, for instance by visually "
#~ "impaired people who do not use a Braille device.  This is triggered "
#~ "simply by typing <literal>s</literal> and <literal>Enter</literal> at the "
#~ "installer boot beep.  More than a dozen languages are supported."
#~ msgstr ""
#~ "&debian; może być zainstalowany przy użyciu syntezatora mowy, np. przez "
#~ "osoby mające problemy ze wzrokiem, które nie korzystają z urządzeń "
#~ "Braille'a. Procedura jest uruchamiana w łatwy sposób, przez wciśnięcie "
#~ "<literal>s</literal>, a następnie <literal>Enter</literal>, gdy "
#~ "instalator wydaje krótkie piknięcie, uruchamiając się. Obsługiwanych jest "
#~ "kilkanaście języków."

#~ msgid "New supported platforms"
#~ msgstr "Nowo obsługiwane platformy"

#~ msgid "Intel Storage System SS4000-E"
#~ msgstr "Intel Storage System SS4000-E"

#~ msgid "Marvell's Kirkwood platform:"
#~ msgstr "Platforma Kirkwood Marvella:"

#~ msgid "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P and TS-419P"
#~ msgstr "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P i TS-419P"

#~ msgid "Marvell SheevaPlug and GuruPlug"
#~ msgstr "Marvell ShevaPlug i GuruPlug"

#~ msgid "Marvell OpenRD-Base, OpenRD-Client and OpenRD-Ultimate"
#~ msgstr "Marvell OpenRD-Base, OpenRD-Client i OpenRD-Ultimate"

#~ msgid "HP t5325 Thin Client (partial support)"
#~ msgstr "HP t5325 Thin Client (obsługa częściowa)"

#~ msgid "Network configuration"
#~ msgstr "Konfiguracja sieci"

#~ msgid "The installer now supports installation on IPv6-only networks."
#~ msgstr ""
#~ "System instalacyjny obsługuje obecnie instalację w sieciach działających "
#~ "wyłącznie z IPv6."

#~ msgid "It is now possible to install over a WPA-encrypted wireless network."
#~ msgstr ""
#~ "Możliwe jest zainstalowanie systemu korzystając z sieci bezprzewodowej z "
#~ "szyfrowaniem WPA."

#~ msgid ""
#~ "<literal>ext4</literal> is the default filesystem for new installations, "
#~ "replacing <literal>ext3</literal>."
#~ msgstr ""
#~ "W nowych instalacjach domyślnym systemem plików stał się <literal>ext4</"
#~ "literal>, zastępując stosowany wcześniej <literal>ext3</literal>."

#~ msgid ""
#~ "The <literal>btrfs</literal> filesystem is provided as a technology "
#~ "preview."
#~ msgstr ""
#~ "System plików <literal>btrfs</literal> jest dostępny jako pokaz "
#~ "przedpremierowy."

#~ msgid ""
#~ "It is now possible to install PCs in UEFI mode instead of using the "
#~ "legacy BIOS emulation."
#~ msgstr ""
#~ "Na zwykłych komputerach możliwa jest instalacja w trybie UEFI zamiast "
#~ "trybu emulacji BIOS-u."

#~ msgid ""
#~ "Asturian, Estonian, Icelandic, Kazakh and Persian have been added to the "
#~ "graphical and text-based installer."
#~ msgstr ""
#~ "Do instalatora graficznego i tekstowego dodano języki: asturyjski, "
#~ "estoński, islandzki, kazaski i perski."

#~ msgid ""
#~ "Thai, previously available only in the graphical user interface, is now "
#~ "available also in the text-based installation user interface too."
#~ msgstr ""
#~ "Język tajski, poprzednio dostępny wyłącznie w trybie graficznym, pojawił "
#~ "się również w wersji tekstowej instalatora."

#~ msgid ""
#~ "Due to the lack of translation updates two languages were dropped in this "
#~ "release: Wolof and Welsh."
#~ msgstr ""
#~ "Z powodu braku aktualizacji, w tym wydaniu porzucono języki: wolof i "
#~ "walijski."
