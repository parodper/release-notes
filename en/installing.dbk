<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
  "https://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
  <!ENTITY % languagedata SYSTEM "language.ent" > %languagedata;
  <!ENTITY % shareddata   SYSTEM "../release-notes.ent" > %shareddata;
]>

<chapter id="ch-installing" lang="en">
<title>Installation System</title>
<para>
The Debian Installer is the official installation system for Debian.  It offers
a variety of installation methods.  The methods that are available to install
your system depend on its architecture.
</para>
<para>
Images of the installer for &releasename; can be found together with
the Installation Guide on the <ulink url="&url-installer;">Debian
website</ulink>.
</para>
<para>
The Installation Guide is also included on the first media of the official
Debian DVD (CD/blu-ray) sets, at:
</para>
<screen>
/doc/install/manual/<replaceable>language</replaceable>/index.html
</screen>
<para>
You may also want to check
the <ulink url="&url-installer;index#errata">errata</ulink> for
debian-installer for a list of known issues.
</para>
<section id="inst-new">
<title>What's new in the installation system?</title>
<para>
There has been a lot of development on the Debian Installer since its previous
official release with &debian; &oldrelease;,
resulting in improved hardware support and
some exciting new features or improvements.
</para>
<para>
If you are interested in an overview of the changes since &oldreleasename;,
please check the release announcements for the &releasename; beta and RC
releases available from the Debian
Installer's <ulink url="&url-installer-news;">news history</ulink>.
</para>

<!-- no major changes -->
<section id="inst-changes" condition="fixme">
<!-- normal title
<title>Major changes</title>
-->
<title>Something</title>
<para>
  Text
</para>
</section>

<!-- (Disable if "no major changes" is in effect again)
release-notes authors should check webwml repo or
https://www.debian.org/devel/debian-installer/

Sources (for bullseye):

https://www.debian.org/devel/debian-installer/News/2019/20191205
https://www.debian.org/devel/debian-installer/News/2020/20200316
https://www.debian.org/devel/debian-installer/News/2020/20201206
https://www.debian.org/devel/debian-installer/News/2021/20210423
https://www.debian.org/devel/debian-installer/News/2021/20210614
https://www.debian.org/devel/debian-installer/News/2021/20210802
- ->

<!- - TODO: Add
*
- ->

<variablelist>

<!- - The following empty paragraph purpose is to unb0rk
     the build until real material get committed - ->

<varlistentry>
<term><!- - Empty Title - -></term>
<listitem>
<para>
<!- - Empty Paragraph - ->
</para>
</listitem>
</varlistentry>

<varlistentry>
<term>New languages</term>
<listitem>
<para>
Thanks to the huge efforts of translators, &debian; can now be installed in 75
languages, including English.
Most languages are available in both the text-based installation
user interface and the graphical user interface, while some
are only available in the graphical user interface.
</para>

<!- - No new languages
<para>
Languages added in this release:
</para>

<itemizedlist>
<listitem>
<para>
</para>
</listitem>
</itemizedlist>
- ->

<para>
The languages that can only be selected using the graphical installer as their
character sets cannot be presented in a non-graphical environment are: Amharic,
Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, Khmer, Malayalam,
Marathi, Nepali, Punjabi, Tamil, Telugu, Tibetan, and Uyghur.
</para>
</listitem>
</varlistentry>

</variablelist>
</section>
-->

</section>

<section id="cloud" arch="amd64;arm64;ppc64el">
  <title>Cloud installations</title>
  <para>
    The <ulink url="&url-cloud-team;">cloud team</ulink> publishes
    Debian &releasename; for several popular cloud computing services
    including:

    <itemizedlist>
      <listitem>
	<para>
	  Amazon Web Services
	</para>
      </listitem>
      <listitem>
	<para>
	  Microsoft Azure
	</para>
      </listitem>
      <listitem>
	<para>
	  OpenStack
	</para>
      </listitem>
      <listitem>
	<para>
	  Plain VM
	</para>
      </listitem>
    </itemizedlist>
  </para>

  <para>
    Cloud images provide automation hooks via <command>cloud-init</command>
    and prioritize fast instance startup using specifically optimized
    kernel packages and grub configurations.  Images supporting
    different architectures are provided where appropriate and the
    cloud team endeavors to support all features offered by the cloud
    service.
  </para>

  <para>
    The cloud team will provide updated images until the end of the
    LTS period for &releasename;. New images are typically released for
    each point release and after security fixes for critical packages.
    The cloud team's full support policy can be found <ulink
    url="&url-cloud-wiki-imagelifecycle;">here</ulink>.
  </para>

  <para>
    More details are available at <ulink
    url="&url-cloud;">cloud.debian.org</ulink> and <ulink
    url="&url-cloud-wiki;">on the wiki</ulink>.
  </para>
</section>

<section id="containers">
  <title>Container and Virtual Machine images</title>
  <para>
    Multi-architecture Debian &releasename; container images are available on
    <ulink url="&url-docker-hub;">Docker Hub</ulink>.  In addition to
    the standard images, a <quote>slim</quote> variant is available that reduces
    disk usage.
  </para>
  <para>
    Virtual machine images for the Hashicorp Vagrant VM manager are
    published to <ulink url="&url-vagrant-cloud;">Vagrant Cloud</ulink>.
  </para>
</section>

</chapter>
