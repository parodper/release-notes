# Translation of old-stuff.po to Brazilian Portuguese language.
# Translation of Debian release notes to Brazilian Portuguese.
# Copyright (C) 2009-2021 Debian Brazilian Portuguese l10n team
# <debian-l10n-portuguese@lists.debian.org>
# This file is distributed under the same license as the Debian release notes.
#
# Translators: Felipe Augusto van de Wiel <faw@debian.org>, -2009.
#              Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2015.
#              Adriano Rafael Gomes <adrianorg@debian.org>, 2017-2019.
# Revisors: Chanely Marques <chanelym@gmail.com>, 2011.
#           Adriano Rafael Gomes <adrianorg@arg.eti.br>, 2013-2015.
#           Tassia Camoes Araujo <tassia@debian.org>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Release Notes\n"
"POT-Creation-Date: 2021-03-20 16:01-0400\n"
"PO-Revision-Date: 2021-03-20 23:33-0400\n"
"Last-Translator: Adriano Rafael Gomes <adrianorg@debian.org>\n"
"Language-Team: l10n Brazilian Portuguese <debian-l10n-portuguese@lists."
"debian.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "pt_BR"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Gerenciando seu sistema &oldreleasename; antes da atualização"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Este apêndice contém informações sobre como assegurar-se de que você "
"consegue instalar ou atualizar pacotes da &oldreleasename; antes de "
"atualizar para a &releasename;. Isso só será necessário em situações "
"específicas."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Atualizando seu sistema &oldreleasename;"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Basicamente, isso não é diferente de qualquer outra atualização do "
"&oldreleasename; que você tenha feito. A única diferença é que você precisa "
"ter certeza de que sua lista de pacotes ainda contém referências para o "
"&oldreleasename; conforme explicado em <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Caso você atualize o seu sistema usando um espelho Debian, ele "
"automaticamente será atualizado para a última versão pontual do "
"&oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr "Verificando seus arquivos source-list do APT"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Se qualquer uma das linhas nos seus arquivos source-list do APT (veja <ulink "
"url=\"&url-man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</"
"ulink>) contiver referências a <quote><literal>stable</literal></quote>, "
"você já está efetivamente <quote>apontando</quote> para a &releasename;. "
"Isso pode não ser o que você quer caso você ainda não esteja pronto para a "
"atualização. Caso você já tenha executado <command>apt update</command>, "
"você ainda pode voltar atrás sem problemas seguindo o procedimento abaixo."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Caso você também já tenha instalado pacotes do &releasename;, provavelmente "
"não há razão para instalar pacotes do &oldreleasename;. Neste caso, você "
"terá que decidir por você mesmo se quer continuar ou não. É possível "
"rebaixar a versão dos pacotes (<quote>downgrade</quote>), mas isso não é "
"abordado neste documento."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Como root, abra o arquivo source-list do APT relevante (tal como <filename>/"
"etc/apt/sources.list</filename>) com seu editor favorito, e verifique todas "
"as linhas começando com <literal>deb http:</literal>, <literal>deb https:</"
"literal>, <literal>deb tor+http:</literal>, <literal>deb tor+https:</"
"literal>, <literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> ou <literal>URIs: tor+https:</literal> "
"para determinar se existe uma referência a <quote><literal>stable</literal></"
"quote>. Caso você encontre qualquer uma, altere de <literal>stable</literal> "
"para <literal>&oldreleasename;</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Caso você tenha linhas começando com <literal>deb file:</literal> ou "
"<literal>URIs: file:</literal>, você mesmo terá que verificar por você mesmo "
"se o local indicado contém um repositório da &oldreleasename; ou da "
"&releasename;."

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Não mude nenhuma linha que comece com <literal>deb cdrom:</literal> ou "
"<literal>URIs: cdrom:</literal>. Fazer isso invalidaria a linha e você teria "
"que executar o <command>apt-cdrom</command> novamente. Não se preocupe se "
"uma linha para uma fonte do tipo <literal>cdrom:</literal> apontar para "
"<quote><literal>unstable</literal></quote>. Embora confuso, isso é normal."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Caso você tenha feito quaisquer mudanças, salve o arquivo e execute"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "para atualizar a lista de pacotes."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Removendo arquivos de configuração obsoletos"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Antes de atualizar o seu sistema para &releasename;, é recomendado remover "
"arquivos de configuração antigos (tais como arquivos <filename>*.dpkg-{new,"
"old}</filename> em <filename>/etc</filename>) do sistema."

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "Atualizar locales antigos para UTF-8"

#~ msgid ""
#~ "Using a legacy non-UTF-8 locale has been unsupported by desktops and "
#~ "other mainstream software projects for a long time. Such locales should "
#~ "be upgraded by running <command>dpkg-reconfigure locales</command> and "
#~ "selecting a UTF-8 default. You should also ensure that users are not "
#~ "overriding the default to use a legacy locale in their environment."
#~ msgstr ""
#~ "Usar um locale não UTF-8 legado deixou de ser suportado por desktops e "
#~ "outros projetos de software bastante conhecidos a um longo tempo. Tais "
#~ "locales devem ser atualizados executando <command>dpkg-reconfigure "
#~ "locales</command> e selecionando um padrão UTF-8. Você também deve se "
#~ "certificar que os usuários não estejam sobrescrevendo o padrão para usar "
#~ "um locale legado nos seus ambientes."

#~ msgid ""
#~ "Lines in sources.list starting with <quote>deb ftp:</quote> and pointing "
#~ "to debian.org addresses should be changed into <quote>deb http:</quote> "
#~ "lines."
#~ msgstr ""
#~ "As linhas no sources.list que iniciem com <quote>deb ftp:</quote> e "
#~ "apontem para endereços debian.org devem ser ser mudadas para linhas "
#~ "<quote>deb http:</quote>."

#~ msgid ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">Debian will remove FTP access to all of its official mirrors on "
#~ "2017-11-01</ulink>.  If your sources.list contains a <literal>debian.org</"
#~ "literal> host, please consider switching to <ulink url=\"https://deb."
#~ "debian.org\">deb.debian.org</ulink>.  This note only applies to mirrors "
#~ "hosted by Debian itself.  If you use a secondary mirror or a third-party "
#~ "repository, then they may still support FTP access after that date.  "
#~ "Please consult with the operators of these if you are in doubt."
#~ msgstr ""
#~ "<ulink url=\"https://lists.debian.org/debian-announce/2017/msg00001.html"
#~ "\">O Debian removerá o acesso via FTP de todos os seus espelhos oficiais "
#~ "em 2017-11-01</ulink>. Se o seu sources.list contém uma máquina "
#~ "<literal>debian.org</literal>, por favor, considere trocar para <ulink "
#~ "url=\"https://deb.debian.org\">deb.debian.org</ulink>. Esta nota se "
#~ "aplica somente a espelhos hospedados pelo próprio Debian. Se você usa um "
#~ "espelho secundário ou um repositório de terceiros, então talvez eles "
#~ "ainda tenham suporte a acesso via FTP depois dessa data. Por favor, "
#~ "consulte os operadores desses serviços em caso de dúvida."

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "No protetor de tela do GNOME, o uso de senhas com caracteres não-ASCII, "
#~ "suporte a pam_ldap, ou até mesmo a capacidade de desbloquear a tela pode "
#~ "não ser confiável quando não estiver usando UTF-8. O leitor de tela do "
#~ "GNOME é afetado pelo bug <ulink url=\"http://bugs.debian."
#~ "org/599197\">#599197</ulink>. O gerenciador de arquivos Nautilus (e todos "
#~ "os programas baseados na glib, e provavelmente também todos os programas "
#~ "baseados na Qt) assume que os nomes de arquivos estão em UTF-8, enquanto "
#~ "o shell assume que eles estão na codificação atual do <quote>locale</"
#~ "quote>. No uso diário, nomes de arquivo não-ASCII são simplesmente "
#~ "inutilizáveis em tais configurações. Além disso, o leitor de tela gnome-"
#~ "orca (o qual permite que usuários com deficiência visual acessem o "
#~ "ambiente de trabalho GNOME) requer um locale UTF-8 desde o Squeeze; sob "
#~ "um conjunto de caracteres antigo, ele será incapaz de ler a informação da "
#~ "tela para elementos do ambiente de trabalho, tais como o Nautilus/Painel "
#~ "do GNOME ou o menu Alt-F1."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Caso o seu sistema esteja localizado e utilizando um locale que não é "
#~ "baseado em UTF-8 você deve fortemente considerar a mudança do seu sistema "
#~ "para utilizar locales do tipo UTF-8. No passado, existiram "
#~ "bugs<placeholder type=\"footnote\" id=\"0\"/> identificados que só se "
#~ "manifestavam ao utilizar um locale não-UTF-8. No ambiente de trabalho, "
#~ "tais locales antigos são suportados através de truques feitos no interior "
#~ "das bibliotecas, e nós não podemos prestar um bom suporte aos usuários "
#~ "que ainda os utilizem."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "Para configurar os locales do seu sistema você pode executar "
#~ "<command>dpkg-reconfigure locales</command>. Certifique-se de selecionar "
#~ "um locale UTF-8 quando for perguntado sobre qual locale utilizar por "
#~ "padrão no sistema. Além disso, você deve rever as configurações do locale "
#~ "dos seus usuários e garantir que eles não tenham definições antigas em "
#~ "seus ambientes de configuração."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "Desde a versão 2:1.7.7-12, o xorg-server não lê mais o arquivo "
#~ "XF86Config-4. Veja também o bug <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
