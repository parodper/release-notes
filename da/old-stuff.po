# Danish translation of release-notes for Debian 7.
# Copyright (C) 2015 Free Software Foundation, Inc.
# Joe Hansen <joedalton2@yahoo.dk>, 2011, 2013, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 7.0 old-stuff.po\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2015-04-04 22:33+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "da"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "Håndter dit &oldreleasename;-system før opgraderingen"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"Dette bilag indeholder information om, hvordan du kontrollerer, at du kan "
"installere eller opgradere pakker fra &oldreleasename; inden du opgraderer "
"til &releasename;. Dette bør kun være nødvendigt i specifikke situationer."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "Opgradering af dit &oldreleasename;-system"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"Det er grundlæggende ikke forskelligt fra enhver anden opgradering af "
"&oldreleasename; som du har udført. Den eneste forskel er, at du først skal "
"sikre dig, at din pakkeliste stadig indeholder referencer til "
"&oldreleasename; som forklaret i <xref linkend=\"old-sources\"/>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"Hvis du opgraderer dit system via et Debianspejl, vil systemet automatisk "
"blive opgraderet til den seneste punktudgave (point release) af "
"&oldreleasename;."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
#, fuzzy
#| msgid "Checking your sources list"
msgid "Checking your APT source-list files"
msgstr "Kontroller din kildeliste"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
#, fuzzy
#| msgid ""
#| "If any of the lines in your <filename>/etc/apt/sources.list</filename> "
#| "refer to <quote><literal>stable</literal></quote>, it effectively points "
#| "to &releasename; already. This might not be what you want if you are not "
#| "ready yet for the upgrade.  If you have already run <command>apt-get "
#| "update</command>, you can still get back without problems by following "
#| "the procedure below."
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"Hvis nogen af linjerne i <filename>/etc/apt/sources.list</filename> "
"refererer til »stable«, <quote>anvender</quote> du allerede &releasename;. "
"Dette er måske ikke, hvad du ønsker, hvis du ikke er klar til opgraderingen "
"endnu. Hvis du allerede har kørt <command>apt-get update</command>, kan du "
"stadig gå baglæns ved at følge nedenstående procedure."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"Hvis du allerede har installeret pakker fra &releasename;, er der ikke "
"længere meget mening i at installere pakker fra &oldreleasename;. I dette "
"tilfælde skal du bestemme dig for, om du vil fortsætte eller ej. Det er "
"muligt at nedgradere pakker, men det beskrives ikke her."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
#, fuzzy
#| msgid ""
#| "Open the file <filename>/etc/apt/sources.list</filename> with your "
#| "favorite editor (as <literal>root</literal>) and check all lines "
#| "beginning with <literal>deb http:</literal> or <literal>deb ftp:</"
#| "literal> for a reference to <quote><literal>stable</literal></quote>.  If "
#| "you find any, change <literal>stable</literal> to "
#| "<literal>&oldreleasename;</literal>."
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"Åbn filen <filename>/etc/apt/sources.list</filename> med din foretrukne "
"tekstbehandler (som <literal>root</literal>) og kontroller alle linjer som "
"begynder med <literal>deb http:</literal> eller <literal>deb ftp:</literal> "
"efter en reference til <quote><literal>stable</literal></quote>. Hvis du "
"finder nogen, så ændr <literal>stable</literal> til <literal>&oldreleasename;"
"</literal>."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
#, fuzzy
#| msgid ""
#| "If you have any lines starting with <literal>deb file:</literal>, you "
#| "will have to check for yourself if the location they refer to contains an "
#| "&oldreleasename; or a &releasename; archive."
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"Hvis du har linjer som begynder med <literal>deb file:</literal>, skal du "
"selv kontrollere om placeringen, som de refererer til indeholder et arkiv "
"for &oldreleasename; eller &releasename;."

# forskrækket/foruroliget
#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
#, fuzzy
#| msgid ""
#| "Do not change any lines that begin with <literal>deb cdrom:</literal>.  "
#| "Doing so would invalidate the line and you would have to run <command>apt-"
#| "cdrom</command> again.  Do not be alarmed if a 'cdrom' source line refers "
#| "to <quote><literal>unstable</literal></quote>.  Although confusing, this "
#| "is normal."
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"Ændr ikke linjer som begynder med <literal>deb cdrom:</literal>. Hvis du gør "
"det, så gøres linjen ugyldig og du skal så køre <command>apt-cdrom</command> "
"igen. Bliv ikke foruroliget hvis en <quote>cdrom</quote>-kildelinje "
"refererer til <quote><literal>unstable</literal></quote>. Selvom det er "
"forvirrende, så er det normalt."

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "Hvis du har foretaget ændringer, så gem filen og kør"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, fuzzy, no-wrap
#| msgid "# apt-get update\n"
msgid "# apt update\n"
msgstr "# apt-get update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "for at opdatere pakkelisten."

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "Fjerner forældede konfigurationsfiler"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"Før du opgraderer dit system til &releasename;, så anbefales det at fjerne "
"gamle konfigurationsfiler (såsom <filename>*.dpkg-{new,old}</filename>-filer "
"under <filename>/etc</filename> fra systemet."

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "Opgrader forældede sprogindstillinger til UTF-8"

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "I GNOME-pauseskærmen er brug af adgangskoder med ikke-ASCII-tegn, "
#~ "pam_ldap-understøttelse, eller endda muligheden for at låse skærmen op, "
#~ "utroværdig når der ikke anvendes UTF8. GNOME-pauseskærmen er påvirket af "
#~ "denne fejl <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>. "
#~ "Nautilus-filhåndteringen (og alle glib-baserede programmer, og "
#~ "sandsynligvis også alle Qt-baserede programmer) antager at filnavne er i "
#~ "UTF-8, mens skallen antager, at de er i den aktuelle sprogindstillings "
#~ "kodning. I daglig brug, er ikke-ASCII-filnavne bare ubrugelige i sådanne "
#~ "opsætninger. Derudover kræver gnome-orca-skærmlæseren (som giver bruger "
#~ "med synsnedsættelse adgang til GNOME-skrivebordsmiljøet) en UTF-8-"
#~ "sprogindstilling siden Squeeze; under et forældet tegnsæt, vil programmet "
#~ "ikke kunne læse vinduesinformation ud for skrivebordselementer såsom "
#~ "Nautilus/GNOME Panel eller Alt-F1-menuen."

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "Hvis dit system er sprogoversat og anvender et sprog, som ikke er baseret "
#~ "på UTF-8, bør du overveje at konvertere dit system til at bruge UTF-8-"
#~ "baserede sprogindstillinger. Tidligere har der været identificeret "
#~ "fejl<placeholder type=\"footnote\" id=\"0\"/>, som kun har vist sig, når "
#~ "der bruges et tegnsæt forskelligt fra UTF-8. På skrivebordet er sådanne "
#~ "forældede sprogindstillinger understøttet via grimme hackninger internt i "
#~ "bibliotekerne, og vi kan ikke yde hjælp til brugere, som stadig anvender "
#~ "dem."

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "For at vælge systemets sprogindstillinger kan du køre <command>dpkg-"
#~ "reconfigure locales</command>. Sikr dig at du vælger en UTF-8-baseret "
#~ "sprogindstilling, når du bliver præsenteret for spørgsmålet om hvilken "
#~ "sprogindstilling, som skal anvendes som standard af systemet. Udover "
#~ "dette bør du kontrollere dine brugeres sprogindstillinger i deres "
#~ "konfigurationsmiljø og sikre dig, at de ikke har forældede "
#~ "sprogdefinitioner i deres konfigurationsmiljø."

#~ msgid ""
#~ "Since release 2:1.7.7-12, xorg-server no longer reads the file "
#~ "XF86Config-4.  See also <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
#~ msgstr ""
#~ "Xorg-server læser siden udgivelse 2:1.7.7-12 ikke længere filen "
#~ "XF86Config-4. Se også <ulink url=\"http://bugs.debian."
#~ "org/619177\">#619177</ulink>."
